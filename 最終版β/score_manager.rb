#require 'rdbi'
#require 'rdbi-driver-sqlite3'

class ScoreManager

  def initialize( dbh, table_name )
    @dbh = dbh
    @table_name = table_name
  end

  def set_up
    @dbh.execute("drop table if exists #{@table_name};") 
    @dbh.execute("create table #{@table_name} (
        id      integer         PRIMARY KEY,
        name    varchar(50)     not null,
        score   integer         not null)")
  end

  def add_score(score)
    printf("名前を入力してください\n")
    user_name = gets.chomp
    @dbh.execute("insert into #{@table_name}(name,score) values(?,?)",
                        user_name,score)
    printf( "登録しました。\nエンターを押してください...")
    gets
  end

  def list_scores(genre)
    printf("#{genre}のスコアリストを表示します。\n")
    list = @dbh.execute("select * from #{@table_name} order by score desc")
    puts "--------------------------"
    list.each{ |value|
      printf("なまえ：%s\nスコア：%d\n",value[1],value[2])
      puts "---------------------------"
    }
    printf( "表示しました。\nエンターを押してください...")
    gets
  end

end

