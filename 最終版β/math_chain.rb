require 'rdbi'
#require 'rdbi-driver-sqlite3'
#require './score_manager.rb'


class MathChain

  
  def initialize( db_name )
    @db_name = db_name
    @dbh = RDBI.connect(:SQLite3, :database=>"#{@db_name}")
  end


  def math_quest

    x = rand(2..9)
    score = 0

    count = 3
    4.times{
     sleep 1
     if count != 0 then
       printf("%d...   ",count)
     else
      printf("GO!!\n")
     end
     count -= 1
    }
    while true
      
      pattern = rand(1..2)
      y = rand(1..9)


      if ( pattern == 1 )&&(x*y < 100)||(x < y) then
        printf("%2d かける %2d = ",x,y)
        z = x*y
      else
        printf("%2d  わる %2d = ",x,y)
        z =x/y
      end

      timer = 0
      #一問ごとのタイマー
      timer_thread = Thread.new {
        printf("\n")
        while true
          sleep 1
          timer += 1
        end
      }

      anser = gets.chomp.to_i
      Thread::kill(timer_thread)
      if anser != z
        printf("\nざんねん！正解は%2d\n",z)
        printf("score:%3d\n",score)
        printf("スコアをきろくする？1.yes/2.no")
        yn = gets.chomp
        if yn == "1"
         @score_mana.add_score(score)
        end
        break;
      end
      
      #if timer <= 0 then
        #score += 10
        #printf("\nうそ…だろ…")
      #elsif timer <= 2
      if timer <= 2 then 
        score += 3
        printf("\n%d秒で正解！速い！",timer)
      elsif timer <= 4 
        score += 2
        printf("\n%d秒で正解！その調子！",timer)
      elsif timer <= 7
        score += 1
        printf("\n%d秒で正解！ギリギリ！",timer) 
      else
        printf("\n正解だけど%d秒で時間オーバー...",timer)
      end

      printf("  げんざいのスコアは%d!\n",score)
      x = z
    end
  end

  def desc
    puts "計算しりとりのルール\n\n"
    puts "自分をこたえるが、次のかけ算かわり算の問題になるゾ！"
    puts "間違えるとゲームオーバー！けど、早く答えるほど点数が高くなるゾ！"
    puts "割り算のあまりは無視（切り下げ）でいいってはっきりわかんだね。"
    printf("\n\nエンターをおしてね")
    gets
  end

  #メニュー部
  def run

    @score_mana = ScoreManager.new(@dbh,"mathscores")
    #@score_mana.set_up
    while true
    printf("\e[H\e[2J")

      puts "=================="
      puts "|| 計算しりとり ||"
      puts "==================\n\n"

      puts "1.スタート/2.ルールせつめい/3.スコアひょうじ/9.もどる"
      menu = gets.chomp
      case menu
      when "1"
        math_quest
      when "2"
        #せつめ
        desc
      when "3"
        #すこあ
        @score_mana.list_scores("算数")
      when "9"
        puts "\e[H\e[2J"
        break;
      end
    end
  end

end

#aaa = MathChain.new("")
#aaa.set_up
#aaa.run
