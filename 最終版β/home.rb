require 'rdbi'
require 'rdbi-driver-sqlite3'
require './math_chain.rb'
require './kokugo_manager.rb'
require './englishquiz.rb'
require './score_manager.rb'

class Home

  def initialize( db_name )
  @db_name = db_name
  @dbh = RDBI.connect(:SQLite3, :database=>"#{@db_name}")
  end

  def clear 
    printf("\e[H\e[2J")
  end


  def run
    score_sub = { :国語 => "kokugoscores", :算数 => "mathscores", :英語 => "englishscores" }

    while true
        clear
        puts "=================="
        puts "|| メニュー画面 ||"
        puts "==================\n\n"

        printf("1.国語\n2.算数\n3.英語\n4.スコア\n9.しゅうりょう\n")
        menu = gets.chomp
        case menu
          when "1"
            clear
            kokugo = KokugoManager.new(@db_name)
            kokugo.run
          when "2"
            clear
            math = MathChain.new(@db_name)
            math.run
          when "3"
            clear
            english = EnglishInfoManager.new(@db_name)
            english.run
            
          when "4"
            while true
              clear
              puts "=================="
              puts "|| スコア表示    ||"
              puts "==================\n\n"

              puts "1.国語/2.算数/3.英語/9.もどる"
              score_menu = gets.chomp

              case score_menu
              when "1"
                @score_mana = ScoreManager.new(@dbh, score_sub[:国語])
                @score_mana.list_scores("国語")
              when "2"
                @score_mana = ScoreManager.new(@dbh, score_sub[:算数])
                @score_mana.list_scores("算数")
              when "3"
                @score_mana = ScoreManager.new(@dbh, score_sub[:英語])
                @score_mana.list_scores("英語")
              when "9"
                break;
              end
            end
          when "9"
            printf("しゅうりょうします...")
            sleep 1
            clear
            break;
        end
    end
  end
end

primary = Home.new("primary.db")
primary.run
