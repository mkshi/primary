#require 'rdbi'
#require 'rdbi-driver-sqlite3'


class KokugoManager
  def initialize(sqlite_name)
    @db_name = sqlite_name
    @dbh = RDBI.connect( :SQLite3, :database => @db_name )
  end

  def start120
    printf ("\n---------------------------------------\n")
    print "・答えの数字を押しましょう。\n・おてつきは3回。\n・3回おてつきしたらゲームオーバーです。"
    puts "\n---------------------------------------"
    item_name = ["上の句","下の句"]
    
    counts = 0
    count = 0

    noids = []

    10.times{
      sql = "select kami,shimo,id from one where id not in (?) order by random() limit 5;"
      #puts "sql=#{sql}"
      #puts "noids.join(',')=#{noids.join(',')}"

      sth = @dbh. execute(sql, noids.join(","))
    
      printf ("\n======================================\n")
      select = Array.new(4)
      col = rand(0..3)
      sth.each_with_index {| value,i |
        if i == 0 then
          puts value[0]
          select[col] = value[1]
          noids.push(value[2])
          puts "======================================"
        else
          select.each_with_index{ | val,j |
            if val == nil then
              select[j] = value[1]
              break
            end
          }
        end
    }
    puts "1.#{select[0]}\n2.#{select[1]}\n3.#{select[2]}\n4.#{select[3]}"
    printf ("\n------------------\n")
    puts "えらんでね"
    puts  "------------------"
    #puts col
    input = gets.chomp.to_i
    if col+1 == input 
      puts "〇"
      counts += 1
    else
      puts "×"
      puts "\n答えは…\n#{select[col] }"
        count += 1
        if count >= 3 
          puts "ゲームオーバー"
          #puts "\n#{counts}問せいかいしました！"
          break
        end 
    end
    }
    puts "\n#{counts}問せいかい！" 
    puts "\n#{count}回おてつきしました"
    printf("スコアをきろくする？1.yes/2.no")
    yn = gets.chomp
    if yn == "1"
      @score_mana.add_score(counts)
    end
  end

  def score
     @score_mana.list_scores("国語")
  end

  def itiran
    printf ("\n------------------\n")
    #item_name = {'id' => "番号", 'kami' => "上の句", 'shimo' => "下の句", 'author' => "歌人" }
    item_name = ["番号","上の句","下の句","歌人" ]
    puts "\n2.百人一首いちらん"
    print "百人一首を全部見る"
    puts "\n-------------------"

    sth = @dbh.execute( "select * from one")
    sth.each do |row|
      row.each_with_index do |val, name|
        puts "#{item_name[name]}: #{val.to_s}"
      end
    end
    sth.finish
    gets
  end
  

  def run 
    @score_mana = ScoreManager.new(@dbh, "kokugoscores")
    @score_mana.set_up
    while true
      printf("\e[H\e[2J")
      print "
1. スタート
2. いちらん表
3. 得点
8. メニューにもどる 
(1,2,3,8)"
      num = gets.chomp
      case
      when '1' == num
        start120
      when '2' == num
        itiran
      when '3' == num
        score
      when '8' == num
        break;
      else
      end
    end
  end

end

#kokugo_manager = KokugoManager.new("hyakunin1-21.db")
#kokugo_manager.run
