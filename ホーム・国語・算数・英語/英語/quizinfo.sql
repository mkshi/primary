 -- 新しく'quizinfos'テーブルを作成する(アプリ納品時1度のみ)
  drop table if exists quizinfos;
  create table quizinfos (
        id              integer      primary key autoincrement not null,
        japanese        varchar(40)  not null,
        english         varchar(40)  not null
        );
