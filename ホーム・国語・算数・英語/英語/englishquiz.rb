#-*- coding: utf-8 -*-
require 'rdbi'       # RDBIを使う
require 'rdbi-driver-sqlite3' # RDBIでSQLite3と接続するドライバ
#require './score_manager.rb'

class EnglishInfos
  def initialize(japanese,english)
    @japanese = japanese
    @english = english
  end

  attr_accessor :japanese, :english

  def to_s
    "#{@japanese}, #{@english}"
  end
 
  def toFormattedString( sep = "\n" )
    "日本語: #{@japanese}#{sep}英語: #{@english}"
  end
end

# EnglishManagerクラスを定義する
class EnglishInfoManager
  def initialize( sqlite_name )
    # SQLiteデータベースファイルに接続
    @db_english = sqlite_name
    @dbh = RDBI.connect( :SQLite3, :database => @db_english)
    @item_name = {:id => "キー", :japanese => "日本語", :english => "英語", }
  end


 # ゲームを始める
  def startgame
    puts"\n \n ☆1.英語虫食いクイズ \n"
    puts"

        +------------------------------------------------+
        |～～１．英語虫食いクイズ 　～～　　        　    |
        |  ~~ルール 説明~~                                |
        |これから日本語のことばがでてくるよ。そして次にで |
        |てきた英語のことばの中に(?)でかくれているところが|
        |あるよ！その(?)にかくれている文字をうめてみよう！|
        |うめたらエンターキーをおしてね！                 |
        | たとえば...                                     |
        |    Q１.りんご appl(?)                           |
        |                                      こたえ：e  |
        |                                                 |
        | クイズは10個あるよ! やってみよう!　             |
        +------------------------------------------------+"
    puts "\n \n クイズがはじまるよ！\n"
    print "\n はじめるときはyという文字をおしてね！\n"
    # 読み込んだ値を大文字に揃える
    yesno = gets.chomp.upcase
    if /^Y$/ =~ yesno
      correct_char = ""
      correct_word = ""
      # Yが1文字のときだけ、ゲームを始める
      sth = @dbh.execute("select japanese, english from quizinfos order by random() limit 10 ")
      columns = sth.schema.columns.to_a
      count = 0 
      sth.each_with_index do |row, row_index|
        puts "\n *---------------------------------*"
        puts "\n もんだい#{row_index + 1}"
        # rowは1件分のデータを保持している
        row.each_with_index do |val, index|
          if :english == columns[index].name
            random_value = rand(val.length)
            correct_char = val[random_value,1]
            correct_word = val.clone
            val[random_value,1] = "(?)"
            puts "英語: #{val}"
          else
            puts "日本語: #{val.to_s}"
          end
        end    
        print "こたえ: "
        answer = gets.chomp
        if answer == correct_char
          count += 10
          puts "\n せいかい！！！ \n"
        else
          puts "\n ざんねん！！まちがいだよ！！正しい答えは#{correct_word}だよ \n"
        end
      end
      print "\n おわりだよ！\n"
      puts   "+--------------------------------------+"
      printf "| あなたのスコアは・・・%3d だよ！！   |\n",count
      puts   "+--------------------------------------+"
        if count == 100 then
          puts "満点だね！！すばらしい！"
        elsif count <= 70 then
          puts "おしい！つぎは満点めざそう！！！"
        else 
          puts "もう一度チャレンジしてみよう！" 
        end
      puts "+-------------------------------------------+"
      printf("今のスコアをきろくしますか? １か２をえらんでね！　1.はい　/ 2. いいえ")
      yes_no = gets.chomp
      if yes_no == "1"
      #  @score_mana.add_score(score)
      end  
    end
  end

    #単語データを登録する
  def addEnglishInfo
    puts "\n.2. 単語データのとうろく \n"
    puts "*---------------------------*"
    puts "単語データをとうろくします。"
    puts "おぼえた単語を入力してみよう"
    puts "*---------------------------*"

    # 英単語データ1件分のインスタンスを作成する
     englishinfos = EnglishInfos.new( "", "", )
    # 登録するデータを項目ごとに入力する
    print "\n"
    print "日本語: " 
    englishinfos.japanese = gets.chomp 
    print "英語: "
    englishinfos.english = gets.chomp
    print "\n"
    # 作成したレストランデータを1件分をデータベースに登録する
    @dbh.execute("insert into quizinfos (japanese,english) values (?,?)", 
            englishinfos.japanese,
            englishinfos.english)
    puts "*---------------------------*"
    puts "\とうろくしました。3の単語データのひょうじで確認できるよ！"
  end

  def listAllEnglishInfos
    # テーブル上の項目名を日本語に変えるハッシュテーブル

    puts "\n 単語データのひょうじ"
    print "とうろくした単語データをかくにんできるよ。"

    puts "\n---------------"
    # テーブルからデータを読み込んで表示する
    sth = @dbh.execute("select * from quizinfos")
    # テーブルの項目名を配列で取得
    columns = sth.schema.columns.to_a
    # select文の実行結果を1件ずつrowに取り出し、繰り返し処理する
    counts = 0
    sth.each do |row|
      # rowは1件分のデータを保持している
      row.each_with_index do |val, index|
        puts "#{@item_name[columns[index].name]}: #{val.to_s}"
      end
      puts "----------"
      counts = counts + 1
    end
      
    # 実行結果を解放する
    sth.finish

    puts "\n#{counts}件表示しました。"
  end
  
  def updateEnglishInfos
    englishinfos = EnglishInfos.new( "", "", )
    print "\n"
    puts "\n4. 単語データのなおし"
    puts "*----------------------------------*"
    print "単語データをなおすことができるよ。"
    print "変更したいキー（すうじ）を押してね："
    key = gets.chomp
    puts "*----------------------------------*" 
    
    before=@dbh.execute("select * from quizinfos where id = ?", key)
#    puts before
    columns = before.schema.columns.to_a
    before.each do |row|
      row.each_with_index do |val, index|
        puts "#{@item_name [columns[index].name]}: #{val.to_s}"
      end
    end
    before.finish


    print "\n"
    print "日本語: "
    englishinfos.japanese = gets.chomp
    print "英語: "
    englishinfos.english = gets.chomp

    # 作成した蔵書データを1件分をデータベースにて修正する
    @dbh.execute("update quizinfos set
                         japanese = ?,
                         english = ?
                         where id = ?",
                         englishinfos.japanese, englishinfos.english, key)
    puts "\n単語データのなおしをしたよ！2.単語データひょうじでかくにんしてみよう！"
  end

  def run
   # @score_mana = SocoreManager.new(@dbh. "mathscores")
    #@score_mana.set_up

    while true
      # 機能選択画面を表示する
      print "
 *1. 英語虫食いクイズ
 *2. 英単語のとうろく 
 *3. 英単語のひょうじ
 *4. 英単語のなおし
 *5. スコアをみる
 *9. はじめにもどる
  数字をえらんでおしてね！(1,2,3,4,5,9)："
      
      # 文字の入力を待つ
      num = gets.chomp
      case 
      when '1' == num
        # 英語虫食いクイズ
        startgame
      when '2' == num
        # 単語データの登録
        addEnglishInfo
      when '3' == num
        # 単語データの表示
        listAllEnglishInfos
      when '4' == num
        # 単語データの修正
        updateEnglishInfos
   #   when '5' == num
   #     @score_mana.list_scores("英語")  
      when '9' == num
        # データベースとの接続を終了する
        @dbh.disconnect
        # アプリケーションの終了
        puts "\n終了しました。"
        break;
      else
        # 処理選択待ち画面に戻る
      end
    end
  end
end

# ここからがアプリケーションを動かす本体

# アプリケーションのインスタンスを作る
# 蔵書データのSQLite3のデータベースを指定している
english_info_manager = EnglishInfoManager.new("english.db")

# BookInfoManagerの処理の選択と選択後の処理を繰り返す
english_info_manager.run
